module.exports = {
    apps: [
        {
            name: 'PBN',
            exec_mode: 'cluster',
            instances: 'max',
            script: './.output/server/index.mjs'
        }
    ]
}

'use strict';

/**
 * post controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::post.post', ({ strapi }) => ({

    async find(ctx) {
        let url = new URL(ctx?.header?.referer || 'http://localhost:3000');
        let host = ctx.host || url.host;
        if (host === 'localhost:1337') host = 'localhost:3000'
        const entity = await strapi.entityService.findMany('api::post.post', {
            populate: {
                category: {
                    fields: ['name', 'slug']
                },
                image: {
                    fields: ['url']
                },
                content: true,
                extended_content: {
                    filters: {
                        $and: [
                            {
                                project: {
                                    name: host
                                }
                            },
                        ],
                    },
                    populate: {
                        image: {
                            fields: ['url']
                        }
                    },
                }
            },
            filters: {
                $and: [
                    {
                        projects: {
                            name: host
                        }
                    },
                ],
            },
        });

        for (const row of entity) {
            if (row.extended_content[0]) {
                for (let [key, value] of Object.entries(row.extended_content[0])) {
                    if (key !== 'id' && value) row[key] = value
                }
            }
            delete row.extended_content
        }

        const sanitizedEntity = await this.sanitizeOutput(entity, ctx);
        return this.transformResponse(sanitizedEntity);
    },

    async findOne(ctx) {
        let url = new URL(ctx?.header?.referer || 'http://localhost:3000');
        const { id } = ctx.params;
        let host = ctx.host || url.host;
        if (host === 'localhost:1337') host = 'localhost:3000'
        const entity = await strapi.entityService.findMany('api::post.post', {
            populate: {
                category: {
                    fields: ['name', 'slug']
                },
                content: true,
                extended_content: {
                    filters: {
                        $and: [
                            {
                                project: {
                                    name: host
                                }
                            },
                        ],
                    },
                    populate: {
                        image: {
                            fields: ['url']
                        }
                    }
                }
            },
            filters: {
                $and: [
                    {
                        projects: {
                            name: host
                        }
                    },
                    {
                        slug: id
                    },
                ],
            },
        });

        for (const row of entity) {
            if (row.extended_content[0]) {
                for (let [key, value] of Object.entries(row.extended_content[0])) {
                    if (key !== 'id' && value) row[key] = value
                }
            }
            delete row.extended_content
        }

        const sanitizedEntity = await this.sanitizeOutput(entity[0], ctx);
        return this.transformResponse(sanitizedEntity);
    }

}));

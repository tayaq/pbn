export default defineNuxtConfig({
    app: {
        head: {
            title: 'PBN',
            meta: [],
            htmlAttrs: {
                'class': 'page'
            },
            bodyAttrs: {
                'class': 'page__body'
            }
        },
    },
    css: ['@/assets/style/style.min.css'],
    runtimeConfig: {
        public: {
            API_BASE_URL: process.env.NUXT_PUBLIC_API_BASE_URL
        },
    },
})
